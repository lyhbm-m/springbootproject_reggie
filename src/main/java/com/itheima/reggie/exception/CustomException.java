package com.itheima.reggie.exception;

//自定义异常类的构造时机？
public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}
