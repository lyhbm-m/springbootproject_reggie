package com.itheima.reggie.common;

import com.itheima.reggie.entity.User;



public class UserHolder {
    private static final ThreadLocal threadlocal=new ThreadLocal();
    public static void setUserId(Long id){
        threadlocal.set(id);
    }
    public static Long getUserId(){
        Long id = (Long) threadlocal.get();
        return id;
    }
    public static void setUser(User user){
        threadlocal.set(user);
    }
    public static User getUser(){
        User user = (User) threadlocal.get();
        return user;
    }
    public static void releaseUser(){
        threadlocal.remove();
    }
}
