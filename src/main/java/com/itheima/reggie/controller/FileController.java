package com.itheima.reggie.controller;

import cn.hutool.core.io.IoUtil;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;


@RestController
@RequestMapping("/common")
@Slf4j
public class FileController {
    @Value("${reggie.path}")
    private String basePath;

    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
//        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
//        log.info(path);
        String substring = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String filename=UUID.randomUUID().toString()+substring;
        File dir = new File(basePath);
        //判断当前目录是否存在
        if(!dir.exists()){
            //目录不存在，需要创建
            dir.mkdirs();
        }
        try {
            file.transferTo(new File(basePath + filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.success(filename);
    }
    @GetMapping("/download")
    public void download(String name, HttpServletResponse res) throws IOException {
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
//        log.info(path);
//        res.setHeader("Content-Disposition",
//        "attachment; filename="+name);// 强制要求浏览器下载。
//        res.setContentType("image/jpeg");// 告知浏览器响应的内容类型

////可以完成添加菜品时的上传图片之后的图片回显功能
//         outputStream= res.getOutputStream();
//         fileInputStream= new FileInputStream(basePath+name);

        ServletOutputStream outputStream = res.getOutputStream();
        FileInputStream fileInputStream = new FileInputStream(new File(basePath+name));
//         //有什么区别？
//        fileInputStream= new FileInputStream(new File(basePath,name));

        IoUtil.copy(fileInputStream,outputStream);
        fileInputStream.close();

//        ServletOutputStream outputStream = response.getOutputStream();
//
//        response.setContentType("image/jpeg");
//
//        int len = 0;
//        byte[] bytes = new byte[1024];
//        while ((len = fileInputStream.read(bytes)) != -1){
//            outputStream.write(bytes,0,len);
//            outputStream.flush();
//        }
//        AbstractApplicationContext

    }
}
