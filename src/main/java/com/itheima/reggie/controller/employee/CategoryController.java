package com.itheima.reggie.controller.employee;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/page")
    public R<Object> selectCategoryByPage(Integer page, Integer pageSize) {
        Page<Category> p = new Page(page, pageSize);
        categoryService.page(p);
        return p.getTotal() > 0 ? R.success(p) : R.error("未知错误");
    }

    @PostMapping
    public R<String> addCategory_aop(@RequestBody Category category, HttpServletRequest req) {
        boolean save = categoryService.save(category);
        return save ? R.success("添加成功") : R.error("添加失败");
    }

    @DeleteMapping
    public R<String> deleteCategoryById(@RequestParam("ids") Long id) {
        return categoryService.deleteCategoryById(id);
    }

// updateById(category);
    @PutMapping
    public R<String> update(@RequestBody Category category){
        categoryService.updateById(category);
        return R.success("修改分类信息成功");
    }

    @GetMapping("/list")
    public R<Object> selectDish(Integer type){
        QueryWrapper<Category> qw = new QueryWrapper<>();
        qw.lambda().eq(type!=null,Category::getType,type);
        qw.lambda().orderByDesc(Category::getSort);
        List<Category> list = categoryService.list(qw);
        return list!=null? R.success(list):R.error("查询失败");
    }

}
