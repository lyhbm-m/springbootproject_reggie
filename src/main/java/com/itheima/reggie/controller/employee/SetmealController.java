package com.itheima.reggie.controller.employee;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("setmeal")
@Slf4j
public class SetmealController {
@Autowired
SetmealService setmealService;

    @GetMapping("/page")
    public R selectSetmealByPage(Integer page, Integer pageSize, String name) {
        LambdaQueryWrapper<Setmeal> lqw = new LambdaQueryWrapper<Setmeal>();
        lqw.like(name != null, Setmeal::getName, name);
        Page p = new Page(page, pageSize);
        Page page1 = setmealService.page(p, lqw);
        return page1 != null ? R.success(page1) : R.error("查询失败！");
    }

    @PostMapping
    public R<String> addSetmeal(@RequestBody SetmealDto setmealDto) {
        R<String> stringR = setmealService.addSetmealWithDish(setmealDto);
        return stringR;
    }

    @DeleteMapping
    public R<String> deleteSetmealById(@RequestParam List<Long> ids){
        log.info("ids:{}",ids);
        return setmealService.deleteSetmealById(ids);
    }
//启用
@PostMapping("/status/{status}")
    public R updateStatusSetmealById(@PathVariable("status") Integer status,@RequestParam List<Long> ids){
    LambdaQueryWrapper<Setmeal> lqw = new LambdaQueryWrapper<>();
    log.info(status.toString());
    List<Setmeal> setmeals = setmealService.listByIds(ids);

    List<Setmeal> collect = setmeals.stream().map(setmeal -> {
        setmeal.setStatus(status);
        return setmeal;
    }).collect(Collectors.toList());
    boolean b = setmealService.updateBatchById(collect);
    return b?R.success(collect):R.error("");
}
//修改回显
@GetMapping("/{id}")
    public R selectSetmealWithDishById(@PathVariable("id") Long id){
    return setmealService.selectSetmealWithDishByIdService(id);
}
//前端
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal){
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null,Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null,Setmeal::getStatus,setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }

}