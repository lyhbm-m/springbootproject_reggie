package com.itheima.reggie.controller.employee;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;


@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee){
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername,employee.getUsername());
        Employee emp = employeeService.getOne(queryWrapper);
        if(emp == null){
            return R.error("登录失败");
        }
        if(!emp.getPassword().equals(password)){
            return R.error("登录失败");
        }
        if(emp.getStatus() == 0){
            return R.error("账号已禁用");
        }
        Long id = emp.getId();
        request.getSession().setAttribute("employee",id);
        log.info(String.valueOf(id));
        emp.setPassword(null);
        return R.success(emp);
    }

//post   request
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request){
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }
//long
    @PostMapping
    public R<String> save_aop(@RequestBody Employee employee, HttpServletRequest req) {
        log.info("新增员工，员工信息：{}",employee.toString());
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        employeeService.save(employee);
        return R.success("添加成功");
    }
//    @GetMapping("/page/{page}/{pageSize}/{name}")
//    public R<Object> selectByPage(@PathVariable int page,@PathVariable int pageSize,@PathVariable String name){
@GetMapping("/page")
public R<Page> page(int page, int pageSize, String name) {
    QueryWrapper<Employee> qw = new QueryWrapper<>();
    qw.lambda().like(name != null, Employee::getName, name);
    //Employee::getUpdateTime
    qw.lambda().orderByDesc(Employee::getUpdateTime);
    Page<Employee> p = new Page<>(page, pageSize);
    employeeService.page(p, qw);//p?
//    return R.success(new Object[]{page1.getRecords(), page1.getTotal()});
    if(p.getTotal()>0){
    return R.success(p);
    }else{
        return R.error("weihzi ");
    }
}
//    @PutMapping
//    public R<String> update(HttpServletRequest request,@RequestBody Employee employee){
//        Long empId = (Long)request.getSession().getAttribute("employee");
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(empId);
//        employeeService.updateById(employee);
//        return R.success("员工信息修改成功");
//    }
    @PutMapping()
    public R<String> updateStatus_aop( @RequestBody Employee employee, HttpServletRequest req) {
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser((long) req.getAttribute("employee"));
//id不存在
        boolean update = employeeService.updateById(employee);
        if (update) {
            return employee.getStatus()==0?R.success("禁用成功！"):R.success("启用成功！");
        }else{
            return R.error("未知错误");
        }
    }
//getById
    @GetMapping("/{id}")
    public R<Employee> update(@PathVariable long id){
//        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();
//        lqw.eq(Employee::getId,id);
//        Employee one = employeeService.getOne(lqw);
        Employee byId = employeeService.getById(id);
        return byId!=null?R.success(byId):R.error("未查询到");
    }

}










