package com.itheima.reggie.controller.employee;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private DishFlavorService dishFlavorService;

    @GetMapping("/page")
    public R selectDishByPage(Integer page,Integer pageSize,String name){
        LambdaQueryWrapper<Dish> lqw = new LambdaQueryWrapper<Dish>();
        lqw.like(name!=null,Dish::getName,name);
        Page p = new Page(page, pageSize);
        Page page1 = dishService.page(p, lqw);
        return page1!=null?R.success(page1):R.error("查询失败！");
//        // 创建page对象，设置当前页和每页大小
//        IPage<Dish> pageInfo = new Page<>(page,pageSize);
//        // 封装查询条件 where name like '%%'
//        LambdaQueryWrapper<Dish> qw = new LambdaQueryWrapper<>();
//        qw.like(StringUtils.isNotEmpty(name),Dish::getName,name);
//        //封装排序
//        qw.orderByDesc(Dish::getUpdateTime);
//        //开始查询
//        dishService.page(pageInfo,qw);
//        long total = pageInfo.getTotal();//总条数
//        List<Dish> records = pageInfo.getRecords();// 查询列表数据
//
//        Map map = new HashMap();
//        map.put("total",total);
//        // records: List<Dish>
//        // 只有菜品分类id：categoryId，没有分类名称
//        // 根据菜品分类id，查询菜品分类数据。在set进去
//
//        // List<Dish> records ---> List<DishDto> dishDtos
//        // for循环的写法：
//        List<DishDto> dishDtos = new ArrayList<>();
//        for (Dish dish : records) {
//            //获取菜品分类id
//            Long categoryId = dish.getCategoryId();
//            Category category = categoryService.getById(categoryId);
//            //获取到了当前分类的名称
//            String categoryName = category.getName();
//            // 新建一个DishDto 将分类名称set进入
//            DishDto dishDto = new DishDto();
//            dishDto.setCategoryName(categoryName);
//            // 还需要将Dish dish 数据放入到 DishDto dishDto 里面
//            BeanUtils.copyProperties(dish,dishDto);
//            dishDtos.add(dishDto);
//        }
//
//        // 拉姆达表达式的写法： List<Dish> records ---> List<DishDto> dishDtos
//        List<DishDto> dishDtos = records.stream().map((dish) -> {
//            Long categoryId = dish.getCategoryId();
//            Category category = categoryService.getById(categoryId);
//            String categoryName = category.getName();
//            DishDto dishDto = new DishDto();
//            dishDto.setCategoryName(categoryName);
//            BeanUtils.copyProperties(dish, dishDto);
//            return dishDto;
//        }).collect(Collectors.toList());
//
//
//        map.put("records",dishDtos);
//        return R.success(map);
    }

    @PostMapping
    public R<String> addDish(@RequestBody DishDto dishDto) {
        R<String> stringR = dishService.addDishWithFlavor_aop1(dishDto);
        return stringR;
    }

//修改回显
    @GetMapping("/{id}")
    public R<DishDto> selectDishById(@PathVariable Long id){
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }
    @PutMapping
    public R<String> updateDish(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());
        return dishService.updateWithFlavor(dishDto);
    }

//新增套餐的关联修改回显+移动端查询菜品分类下的菜品
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish) {
        Long categoryId = dish.getCategoryId();
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(categoryId != null, Dish::getCategoryId, categoryId);
        queryWrapper.eq(Dish::getStatus,dish.getStatus());
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> dishes = dishService.list(queryWrapper);

        List<DishDto> dishDtos=dishes.stream().map(dish1 ->{
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish1,dishDto);

            DishFlavor dishFlavor = new DishFlavor();
            LambdaQueryWrapper<DishFlavor> lqw = new LambdaQueryWrapper<>();
            lqw.eq(DishFlavor::getDishId,dish1.getId());
            List<DishFlavor> flavors = dishFlavorService.list(lqw);
            dishDto.setFlavors(flavors);
            return dishDto;
        }).collect(Collectors.toList());

        return R.success(dishDtos);
    }

}
