package com.itheima.reggie.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.itheima.reggie.common.R;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;


    @Slf4j
    @RestController
    @RequestMapping("/addressBook")
    public class AddressBookController {
        @Autowired
        private AddressBookService addressBookService;
//         查询指定用户的全部地址
        @GetMapping("/list")
        public R<List<AddressBook>> list(AddressBook addressBook) {
            Long id = UserHolder.getUser().getId();
            LambdaQueryWrapper<AddressBook> qw = new LambdaQueryWrapper<>();
            log.info("qiguai:{}",UserHolder.getUser().getId());
            qw.eq(id!=null,AddressBook::getUserId,id);
            qw.orderByDesc(AddressBook::getUpdateTime);
            List<AddressBook> list = addressBookService.list(qw);
            return list!=null?R.success(list):R.error("");
        }
//      新增
        @PostMapping
        public R<AddressBook> save(@RequestBody AddressBook addressBook) {
            addressBook.setUserId(UserHolder.getUser().getId());
//            StringBuilder stringBuilder = new StringBuilder();
//            if(one.getProvinceName()!=null) stringBuilder.append(one.getProvinceName());
//            if(one.getCityName()!=null)   stringBuilder.append(one.getCityName());
//            if(one.getDistrictName()!=null)  stringBuilder.append(one.getDistrictName());
//            orders.setAddress(stringBuilder.toString());
            addressBookService.save(addressBook);
            return R.success(addressBook);
        }
        //删除
        @DeleteMapping()
        public R get(@RequestParam("ids") Integer id) {
            boolean remove = addressBookService.removeById(id);
            return remove?R.success(""):R.error("");
        }
//          根据id查询地址
        @GetMapping("/{id}")
        public R get(@PathVariable Long id) {
            AddressBook addressBook = addressBookService.getById(id);
            return addressBook!=null?R.success(addressBook):R.error("没有找到该对象");
        }
        //修改
        @PutMapping()
        public R updateAddressBook(@RequestBody AddressBook addressBook) {
            LambdaUpdateWrapper<AddressBook> lqw = new LambdaUpdateWrapper<>();
            lqw.eq(AddressBook::getId, addressBook.getId());
            boolean update = addressBookService.update(addressBook, lqw);
            return update?R.success(""):R.error("");
        }
//        设置默认地址
        @PutMapping("default")
        public R<AddressBook> setDefault(@RequestBody AddressBook addressBook) {
            LambdaUpdateWrapper<AddressBook> lqw = new LambdaUpdateWrapper<>();
            lqw.eq(AddressBook::getUserId, UserHolder.getUser().getId());
            lqw.set(AddressBook::getIsDefault, 0);
            addressBookService.update(lqw);
            addressBook.setIsDefault(1);
            addressBookService.updateById(addressBook);
            return R.success(addressBook);
        }
//         查询默认地址
        @GetMapping("default")
        public R<AddressBook> getDefault() {
            LambdaQueryWrapper<AddressBook> qw = new LambdaQueryWrapper<>();
            qw.eq(AddressBook::getUserId,UserHolder.getUser().getId());
            qw.eq(AddressBook::getIsDefault, 1);
            AddressBook addressBook = addressBookService.getOne(qw);
            return null == addressBook?R.error("没有找到该对象"):R.success(addressBook);
        }
    }

