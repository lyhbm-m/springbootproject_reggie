package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.exception.CustomException;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper,Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;

    @Transactional
    @Override
    public R<String> addSetmealWithDish(SetmealDto setmealDto) {
        boolean save = this.save(setmealDto);
//菜品id
        Long setmealId = setmealDto.getId();

        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
//        //底层迭代器，并发修改
//        for (SetmealDish setmealDish : setmealDishes) {
//            setmealDish.setSetmealId(setmealId);
//        }
        //
//          setmealDishes.stream().forEach(new Consumer<SetmealDish>() {
//              @Override
//              public void accept(SetmealDish setmealDish) {
//                  setmealDish.setSetmealId(setmealId);
//              }
//          });
        setmealDishes = setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealId);
            return item;
        }).collect(Collectors.toList());
        boolean b = setmealDishService.saveBatch(setmealDishes);

        return save|b?R.success(""):R.error("");
    }

    @Transactional
    @Override
    public R deleteSetmealById(List<Long> ids) {
        //select count(*) from setmeal where id in (1,2,3) and status = 1
        //查询套餐状态，确定是否可用删除
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(queryWrapper);
        if(count > 0){
            //如果不能删除，抛出一个业务异常
            throw new CustomException("套餐正在售卖中，不能删除");
        }
        //如果可以删除，先删除套餐表中的数据---setmeal
        boolean remove = this.removeByIds(ids);
        //delete from setmeal_dish where setmeal_id in (1,2,3)
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
        //删除关系表中的数据----setmeal_dish
        boolean remove1 = setmealDishService.remove(lambdaQueryWrapper);
        return remove|remove1?R.success(""):R.error("");
    }
//修改回显
    @Override
    public R selectSetmealWithDishByIdService(Long id) {
//1、根据套餐id查询套餐数据 select * from setmeal where id =?
        Setmeal setmeal = this.getById(id);
//2、根据套餐id查询套餐对应的菜品数据 select * from setmeal_dish where setmeal_id = ?
        LambdaQueryWrapper<SetmealDish> lqw = new LambdaQueryWrapper<>();
        lqw.eq(SetmealDish::getSetmealId,id);
        List<SetmealDish> setmealDishes = setmealDishService.list(lqw);

//3、将套餐 setmeal 和套餐菜品集合 setmealDishList 数据封装到 SetmealDto并返回
// 3.0 首先new一个SetmealDto对象

// 3.1、BeanUtils.copyProperties() 拷贝setmeal到setmealDto中

// 3.2、将setmealDishList 设置到 setmealDto中

        SetmealDto setmealDto = new SetmealDto();
        setmealDto.setSetmealDishes(setmealDishes);
        return setmeal!=null?R.success(setmealDto):R.error("");
//4、返回 setmealDto
//        return R.success(setmealDto);
    }
}





// 请求方式：POST
//
// 请求参数： ?ids=1415580119015145474,1505375213336915969

//warning: LF will be replaced by CRLF in reggie_project_practice/src/main/webapp/WEB-INF/web.xml.bak.
//        The file will have its original line endings in your working directory
