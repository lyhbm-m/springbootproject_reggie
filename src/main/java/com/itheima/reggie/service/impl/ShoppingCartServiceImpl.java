package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.ShoppingCartMapper;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    UserMapper userMapper;
//    @Override
    public void submit(Orders orders) {
        // orders  order_detail
        // 0、获取用户id
        Long userId = UserHolder.getUserId();
        // 1、查询购物车数据：select * from shopping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<ShoppingCart> eq = qw.eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectList(qw);

        // 2、查询用户数据 select * from user where id = ?
        User user = userMapper.selectById(userId);

        // 3、根据地址id查询地址数据 select * from address_book where id = ?
//        AddressBook addressBook = addressBookMapper.selectById(orders.getAddressBookId());
//
//        long orderId = IdWorker.getId();//订单号
//
//        BigDecimal amount = new BigDecimal(0);//总价
//        // 4、保存订单明细数据：点了哪些菜品或套餐
//        for (ShoppingCart shoppingCart : shoppingCarts) {
//            // ShoppingCart shoppingCart --> OrderDetail orderDetail
//            OrderDetail orderDetail = new OrderDetail();
//            BeanUtils.copyProperties(shoppingCart,orderDetail);
//            // orderId单独设置
//
//            orderDetail.setOrderId(orderId);
//            // amount 金额单独设置 数量  *  单价
//            BigDecimal price = shoppingCart.getAmount();
//            BigDecimal number = new BigDecimal(shoppingCart.getNumber());
//            orderDetail.setAmount(price.multiply(number));
//            amount.add(orderDetail.getAmount());//
//            orderDetailMapper.insert(orderDetail);
//        }
//        // 5、保存订单数据，有许多数据需要单独设置
//        orders.setId(orderId);//订单id
//        orders.setOrderTime(LocalDateTime.now());//订单创建时间
//        orders.setCheckoutTime(LocalDateTime.now());//订单结账时间
//        orders.setStatus(2);//1待付款，2待派送，3已派送，4已完成，5已取消
//        orders.setAmount(amount);//总金额
//        orders.setUserId(userId);//用户id
//        orders.setNumber(String.valueOf(orderId));//订单号
//        orders.setUserName(user.getName());//用户名
//        orders.setConsignee(addressBook.getConsignee());//收货人
//        orders.setPhone(addressBook.getPhone());//收货人联系电话
//        orders.setAddress(
//                (addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
//                        + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
//                        + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
//                        + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));//省+市+区域+详细地址
//        orderMapper.insert(orders);
//        // 6、删除当前用户的购物车数据
//        // delete from shopping_cart where user_id = ?
//        LambdaQueryWrapper<ShoppingCart> qw1 = new LambdaQueryWrapper<>();
//        qw1.eq(ShoppingCart::getUserId,userId);
//        shoppingCartMapper.delete(qw1);
    }
}
