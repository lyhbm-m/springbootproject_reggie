package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.exception.CustomException;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

     @Autowired
     DishService dishService;
     @Autowired
     SetmealService setmealService;

     @Override
     public R<String> deleteCategoryById(Long id){
          LambdaQueryWrapper<Dish> lqwD = new LambdaQueryWrapper<>();
          lqwD.eq(Dish::getCategoryId,id);
          int countD = dishService.count(lqwD);

          LambdaQueryWrapper<Setmeal> lqwS = new LambdaQueryWrapper<>();
          lqwS.eq(Setmeal::getCategoryId,id);
          int countS =setmealService.count(lqwS);

//          LambdaQueryWrapper<Category> lqwC = new LambdaQueryWrapper<>();
//          lqwC.eq(Category::getId,id);

          if (countD > 0) {
               throw new CustomException("当前分类下关联了菜品，不能删除");
          } else if (countS > 0) {
               throw new CustomException("当前分类下关联了套餐，不能删除");
          } else {
//               boolean remove = categoryService.remove(lqwC);
//               boolean remove = categoryService.removeById(id);
               boolean remove = super.removeById(id);
               return remove ? R.success("删除成功！") : R.error("删除失败！");
          }
     }

}
