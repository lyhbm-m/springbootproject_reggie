package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.R;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.OrdersMapper;
import com.itheima.reggie.service.AddressBookService;
import com.itheima.reggie.service.OrderDetailService;
import com.itheima.reggie.service.OrdersService;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.lang.Long.parseLong;

@Service
@Slf4j
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper,Orders> implements OrdersService {
    @Autowired
    AddressBookService addressBookService;
    @Autowired
    ShoppingCartService shoppingCartService;
    @Autowired
    OrderDetailService orderDetailService;
//    @Autowired
//    OrderDetail orderDetail;
//ShoppingController shoppingController;

    @Transactional
    @Override
    public R submitOrderService(Orders orders) {
        orders.setOrderTime(LocalDateTime.now());
        orders.setUserId(UserHolder.getUser().getId());
        orders.setUserName(UserHolder.getUser().getName());
        orders.setAddressBookId(orders.getAddressBookId());
//        AddressBook one = addressBookService.getById(orders.getAddressBookId());
//        one.setId(null);
//        BeanUtils.copyProperties(one,orders);
       //购物车算出总额
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId, UserHolder.getUser().getId());
        List<ShoppingCart> list = shoppingCartService.list(lqw);
        AtomicInteger n= new AtomicInteger();
        list.stream().map((item)->{
            n.addAndGet(n.addAndGet(item.getNumber()*Integer.parseInt(item.getAmount().toString())));

            return n;
        });

        //地址
        LambdaQueryWrapper<AddressBook> lqw1 = new LambdaQueryWrapper<>();
        lqw1.eq(AddressBook::getUserId,UserHolder.getUser().getId());
        AddressBook one = addressBookService.getOne(lqw1);

        StringBuilder stringBuilder = new StringBuilder();
                if(one.getProvinceName()!=null) stringBuilder.append(one.getProvinceName());
                if(one.getCityName()!=null)   stringBuilder.append(one.getCityName());
                if(one.getDistrictName()!=null)  stringBuilder.append(one.getDistrictName());
        orders.setAddress(stringBuilder.toString());

        orders.setAmount(new BigDecimal(n.toString()));
        orders.setCheckoutTime(LocalDateTime.now());

        log.info("此时orders的id:{}",orders.getId());
        this.save(orders);
        log.info("此时orders的id:{}",orders.getId());
//orderDetail
        List<OrderDetail> orderDetails = list.stream().map((item) -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(orders.getId());
            item.setId(null);//自动装箱？ Long.parseLong(null)
            BeanUtils.copyProperties(item, orderDetail);
            return orderDetail;
        }).collect(Collectors.toList());

//        orders.getNumber()

        orderDetailService.saveBatch(orderDetails);
        shoppingCartService.remove(lqw);
        return R.success(orders);
    }
}
