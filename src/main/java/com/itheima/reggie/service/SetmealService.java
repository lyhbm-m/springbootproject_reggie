package com.itheima.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    R<String> addSetmealWithDish(SetmealDto setmealDto);


    R deleteSetmealById(List<Long> ids);

    R selectSetmealWithDishByIdService(Long id);
}