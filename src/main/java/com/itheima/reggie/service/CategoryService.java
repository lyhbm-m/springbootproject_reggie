package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;

public interface CategoryService extends IService<Category> {
        //根据ID删除分类
        public R<String> deleteCategoryById(Long id);
}
