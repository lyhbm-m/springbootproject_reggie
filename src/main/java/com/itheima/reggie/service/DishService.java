package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;

public interface DishService extends IService<Dish> {
    R<String> addDishWithFlavor_aop1(DishDto dishDto);

    DishDto getByIdWithFlavor(Long id);

    R updateWithFlavor(DishDto dishDto);
}