package com.itheima;

import com.itheima.reggie.ReggieApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

@SpringBootTest(classes = ReggieApplication.class)
@Slf4j
public class TestBean {
    @Test
    public void getBean(){
        GenericApplicationContext context = new GenericApplicationContext();
        AnnotationConfigUtils.registerAnnotationConfigProcessors(context.getDefaultListableBeanFactory());
        context.registerBean("bean1",Bean1.class,beanDefinition->{
//            beanDefinition.getPropertyValues().add("bean3",new RuntimeBeanReference("bean2"));
            ((RootBeanDefinition)beanDefinition).setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME);
        });context.registerBean("bean2",Bean2.class);
        context.registerBean("bean3",Bean3.class);context.registerBean("bean4",Bean4.class);
        context.refresh();
    }
}
@Slf4j
class Bean1{
    MyBean bean;
    @Autowired @Qualifier("bean2")
    public void setBean3(MyBean bean){ log.info("被实例化的bean为：{}",bean.toString());this.bean=bean; }
}
interface MyBean{}
class Bean2 implements MyBean{}
class Bean3 implements MyBean{}
class Bean4 implements MyBean{}